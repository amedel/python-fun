import json
import time


def ceroCheckAndAppendIfNecessary(jsonObjectFromFile, pricesListFiltered):
  if('price' in jsonObjectFromFile and jsonObjectFromFile['price']!=0.0):
    pricesListFiltered.append(jsonObjectFromFile['price'])

print("lets start!, I need manufacturer's name(ENTER for all):")
carMaker=raw_input()
if(carMaker!=""):
  print("ok, now I need car's model(ENTER for all):")
  carModel=raw_input()
  if(carModel!=""):
    print("do you have a year in mind?(ENTER for all):")
    carYear=raw_input()
    if(carYear!=""):
      print("do you want to indicate a region or city?(ENTER for all)")
      userRegionCity=raw_input()

print("please wait...")

pricesListFiltered=[]
horaDeComienzo=time.time()
with open('./cars.json', 'rb') as ficheroJson:
  for renglon in ficheroJson:
    if renglon: 
      jsonObjectFromFile=json.loads(renglon)

      if(carMaker==""):
        ceroCheckAndAppendIfNecessary(jsonObjectFromFile, pricesListFiltered)
        continue
      if('make' in jsonObjectFromFile and jsonObjectFromFile['make'].lower()==carMaker.lower()):
        if(carModel==""):
          ceroCheckAndAppendIfNecessary(jsonObjectFromFile, pricesListFiltered)
          continue
        if('model' in jsonObjectFromFile and jsonObjectFromFile['model'].lower()==carModel.lower()):
          if(carYear==""):
            ceroCheckAndAppendIfNecessary(jsonObjectFromFile, pricesListFiltered)
            continue
          if('year' in jsonObjectFromFile and jsonObjectFromFile['year']==int(carYear)):
            if(userRegionCity==""):
              ceroCheckAndAppendIfNecessary(jsonObjectFromFile, pricesListFiltered)
              continue   
            if(('region' in jsonObjectFromFile and jsonObjectFromFile['region'].lower()==userRegionCity.lower())
                or ('city' in jsonObjectFromFile and jsonObjectFromFile['city'].lower()==userRegionCity.lower())):
              ceroCheckAndAppendIfNecessary(jsonObjectFromFile, pricesListFiltered)
              continue
                        
print("loading file time: " + str(time.time()-horaDeComienzo))
horaDeComienzo=time.time()
if(len(pricesListFiltered)>0):
   print("Average price for the last few months: " + str(sum(pricesListFiltered)/len(pricesListFiltered)))
else:
   print("Don't have enough information for your search(zero results), sorry")
#print("getting average price time:" + str(time.time()-horaDeComienzo))
