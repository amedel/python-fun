import json
import datetime
import dateutil.parser
import time
import unicodedata
import random

contentIdscatalog = {} 
startTime=time.time()
with open('./cars.json', 'rb') as jsonFile:
    for line in jsonFile:
        if line: 
	    jsonObjectParsedFromFile=json.loads(line)
	    dictionaryKey=unicodedata.normalize("NFD", jsonObjectParsedFromFile['contentChunk'].replace(" ","")) 
            dictionaryValue=jsonObjectParsedFromFile['uniqueId']
            if(dictionaryKey in contentIdscatalog):
              contentIdscatalog[dictionaryKey].append(dictionaryValue)
            else:
              contentIdscatalog.update({dictionaryKey:[dictionaryValue]})
        else:
            break

print("Filling catalog time: " + str(time.time()-startTime))

duplicateIdsSetsToShow=0
for key, value in contentIdscatalog.items():
    if len(value) > 1:
       if(bool(random.getrandbits(1)) and len(value)<4):
          print(value)
          duplicateIdsSetsToShow+=1
       if(duplicateIdsSetsToShow>4):
          break
      
print("showing samples time:" + str(time.time()-startTime))
	
